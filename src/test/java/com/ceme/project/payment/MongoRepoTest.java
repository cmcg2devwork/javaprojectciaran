package com.ceme.project.payment;

import com.ceme.project.payment.dao.IPaymentsData;
import com.ceme.project.payment.entity.Payment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@SpringBootTest
public class MongoRepoTest {
    @Autowired
    IPaymentsData dao;

    @Test
    public void testSavePaymentDocument()
    {

        //default time zone
        ZoneId defaultZoneId = ZoneId.systemDefault();

        //creating the instance of LocalDate using the day, month, year info
        LocalDate localDate = LocalDate.of(2019, 9, 14);

        //local date + atStartOfDay() + default time zone + toInstant() = Date
        Date paymentDate = Date.from(localDate.atStartOfDay(defaultZoneId).toInstant());

        Payment payment = new Payment( 10, paymentDate, "0", 200, 220);

        dao.save(payment);

        payment = dao.findById(10);

        Assert.notNull(payment, "Check if Payment is found");

    }


}
