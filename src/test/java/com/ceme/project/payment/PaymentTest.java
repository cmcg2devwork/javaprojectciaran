package com.ceme.project.payment;

import com.ceme.project.payment.entity.Payment;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

public class PaymentTest {

//    @Test
//    public void testSetPaymentGeneratesAnException() {
//
//        //default time zone
//        ZoneId defaultZoneId = ZoneId.systemDefault();
//
//        //creating the instance of LocalDate using the day, month, year info
//        LocalDate localDate = LocalDate.of(2019, 11, 21);
//
//        //local date + atStartOfDay() + default time zone + toInstant() = Date
//        Date paymentDate = Date.from(localDate.atStartOfDay(defaultZoneId).toInstant());
//
//        Payment homeInsurancePayment = new Payment( 55, paymentDate, "new", -100, 220);
//
//        try {
//            homeInsurancePayment.setAmount(-100);
//        } catch (PaymentException e) {
//            e.printStackTrace();
//        }
//
//        Assert.state((homeInsurancePayment.getAmount() == -100), "Check that getAmount is - \"PaymentException <0\"");
//
//    }

    @Test()
    public void testConstructor()
    {

        //default time zone
        ZoneId defaultZoneId = ZoneId.systemDefault();

        //creating the instance of LocalDate using the day, month, year info
        LocalDate localDate = LocalDate.of(2019, 11, 21);

        //local date + atStartOfDay() + default time zone + toInstant() = Date
        Date paymentDate = Date.from(localDate.atStartOfDay(defaultZoneId).toInstant());

        Payment homeInsurancePayment = new Payment( 55, paymentDate, "homeInsurance", 200, 220);

        Assert.state((homeInsurancePayment.getType()=="homeInsurance"), "Check that type is - \"homeInsurance\"");


    }

}
