package com.ceme.project.payment.entity;

import com.ceme.project.payment.exceptions.PaymentException;
import org.springframework.data.annotation.Id;

import java.util.Date;

public class Payment {
    @Id
    private int id;
    private Date paymentDate;
    private String type;
    private double amount;
    private int customerId;

    public Payment(int id, Date paymentDate, String type, double amount, int customerId) {
        this.id = id;
        this.paymentDate = paymentDate;
        this.type = type;
        this.amount = amount;
        this.customerId = customerId;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) throws PaymentException {

        if(amount<=0 )
        {
            throw new PaymentException("Payment < 0");
        }
        else
        {
            this.amount = amount;
        }
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", paymentDate=" + paymentDate +
                ", type='" + type + '\'' +
                ", amount=" + amount +
                ", customerId=" + customerId +
                '}';
    }


}
