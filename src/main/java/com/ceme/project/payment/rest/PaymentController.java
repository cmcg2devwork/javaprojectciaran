package com.ceme.project.payment.rest;

import com.ceme.project.payment.entity.Payment;
import com.ceme.project.payment.service.IPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api")
public class PaymentController {

    @Autowired
    IPaymentService service;

    @RequestMapping(value = "/rowcount", method = RequestMethod.GET)
    public long rowcount()
    {
        return  (int)service.rowcount();
    }

    @RequestMapping(value = "/findById", method = RequestMethod.GET)
    public Payment findById(int id)
    {
        return  service.findById(id);
    }

    @RequestMapping(value = "/findByType", method = RequestMethod.GET)
    public List<Payment> findByType(String type)
    {
        return  service.findByType(type);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public void save(@RequestBody Payment payment)
    {
        service.save(payment);
    }


}
