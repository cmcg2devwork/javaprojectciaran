package com.ceme.project.payment.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class StatusController {

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getStatus() {
        return "Rest Api is running";
    }
}

