package com.ceme.project.payment.exceptions;

public class PaymentException extends Throwable {

    private static final long serialVersionUID = 3538181174213580960L;

    public PaymentException(String message)
    {
        super(message);
    }
}

//public class EmployeeException extends RuntimeException {
//    /**
//     *
//     */
//    private static final long serialVersionUID = 3538181174213580960L;
//
//    public EmployeeException(String message)
//    {
//        super(message);
//    }
//
//
//}