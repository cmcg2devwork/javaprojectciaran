package com.ceme.project.payment.service;

import com.ceme.project.payment.dao.IPaymentsData;
import com.ceme.project.payment.entity.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService implements IPaymentService{

    @Autowired
    private IPaymentsData dao;

    @Override
    public long rowcount(){
        return dao.rowcount();
    }

    @Override
    public Payment findById(int id)
    {
        if (id<=0)
        {
            return null;
        }
        else
        {
            return dao.findById(id);
        }

    }

    @Override
    public List<Payment> findByType(String type)
    {
        return dao.findByType(type);
    }

    @Override
    public void save(Payment payment)
    {
        dao.save(payment);
    }

}

