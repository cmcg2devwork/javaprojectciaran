package com.ceme.project.payment.service;

import com.ceme.project.payment.entity.Payment;

import java.util.List;

public interface IPaymentService {
    long rowcount();

    Payment findById(int id);

    List<Payment> findByType(String type);

    void save(Payment payment);


}
