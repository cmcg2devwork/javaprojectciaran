package com.ceme.project.payment.dao;

import com.ceme.project.payment.entity.Payment;

import java.util.List;

public interface IPaymentsData {
        Long rowcount();
        Payment findById(int id);
        List<Payment> findByType(String type);
        void save(Payment employee);

}
