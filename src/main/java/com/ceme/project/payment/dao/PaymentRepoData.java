package com.ceme.project.payment.dao;

import com.ceme.project.payment.entity.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentRepoData implements IPaymentsData{

    @Autowired
    private MongoTemplate tpl;

    @Override
    public Long rowcount() {
        Query query = new Query();
        return tpl.count(query, Payment.class);
    }

    @Override
    public Payment findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        return tpl.findOne(query, Payment.class);
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        return tpl.find(query, Payment.class);
    }

    @Override
    public void save(Payment payment) {
        tpl.insert(payment);
    }

}
